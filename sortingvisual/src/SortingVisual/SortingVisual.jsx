import react from 'react';
import {SaveAnimationMergeSort} from '../SortingAlgo/SortingAlgo.js';
import './SortingVisual.css';

export default class SortingVisual extends react.Component {
    constructor(props) {
        super(props);

        this.state={
            array: [],
            sizeArray: 0,
        };
    }

    componentDidMount(){
        this.resetArray();
    }

    resetArray(){
        console.log(this.state.sizeArray)

        const array = []
        for(let i = 0; i < this.state.sizeArray;i++){
            array.push(randomIntFromInterval(5,730));
        }
        this.setState({array : array});
    }

    mergeSort() {
        const animations = SaveAnimationMergeSort(this.state.array);
        for (let i = 0; i < animations.length; i++) {
          const arrayBars = document.getElementsByClassName('array-bar');
          const isColorChange = i % 3 !== 2;
          if (isColorChange) {
            const [barOneIdx, barTwoIdx] = animations[i];
            const barOneStyle = arrayBars[barOneIdx].style;
            const barTwoStyle = arrayBars[barTwoIdx].style;
            const color = i % 3 === 0 ? 'red' : 'turqoise';
            setTimeout(() => {
              barOneStyle.backgroundColor = color;
              barTwoStyle.backgroundColor = color;
            }, i * 3);
          } else {
            setTimeout(() => {
              const [barOneIdx, newHeight] = animations[i];
              const barOneStyle = arrayBars[barOneIdx].style;

              barOneStyle.height = `${newHeight}px`;
            }, i * 3);
          }
          

        }
        
      }
    quickSort(start, end) { /*
        const animations = SortingAlgo.quickSort( this.state.array) ;
        const newAnimations = [];
            if (array.length < 2) {
              return array
            }
            const chosenIndex = array.length - 1
            const chosen = array[chosenIndex]
            const a = []
            const b = []
            for (let i = 0; i < chosenIndex; i++) {
              const temp = array[i]
              temp < chosen ? a.push(temp) : b.push(temp)
            }
          
            const output = [...quickSort(a), chosen, ...quickSort(b)]
            console.log(output.join(' '))
            return output
          
          
          quickSort(numbers)

          */
         this.quick_sort(0,this.state.array.length-1);
    }

    div_update(cont,height,color){
        setTimeout(function(){
            cont.style="height:" + height + "; background-color:" + color + ";";
        },1);
    }
     quick_sort (start, end )
    {
        if( start < end )
        {
            //stores the position of pivot element
            var piv_pos = this.quick_partition (start, end ) ;     
            this.quick_sort (start, piv_pos -1);//sorts the left side of pivot.
            this.quick_sort (piv_pos +1, end) ;//sorts the right side of pivot.
        }
     }
    quick_partition (start, end){
        var i = start + 1;
        
        const arrayBars = document.getElementsByClassName('array-bar');
        const barOneStyle = arrayBars[start].style
        var piv = barOneStyle.height ;//make the first element as pivot element.


            for(var j =start + 1; j <= end ; j++ )
            {
                //re-arrange the array by putting elements which are less than pivot on one side and which are greater that on other.
                if (arrayBars[ j ].style.height < piv)
                {   
                    const bar2 = arrayBars[i].style
                    const bar3 = arrayBars[j].style
                    console.log(bar2.height)
                    setTimeout(() => {
                        
                        var temp=bar2.height;
                        
                        bar2.height = bar3.height;
                        bar3.height = temp
                    },1)
                    

                    i += 1;
                }
        }
        const bar4 = arrayBars[start].style
        const bar5 = arrayBars[i-1].style
        setTimeout(()=>{
            
                        var temp=bar4.height;
                        
                        bar4.height = bar5.height;
                        bar5.height = temp

        },1)
        


        for(var t=start;t<=i;t++)
        {
            
        }

        return i-1;//return the position of the pivot
    }
   
    
    testSortingAlgo(){
        for (let i = 0; i < 100; i++) {
            const array = [];
            const length = randomIntFromInterval (1, 1000) ;
            for (let i = 0; i < length; i++) {
                array.push(randomIntFromInterval(-1000, 1000));
            }
            const javaScriptsortedArray = array.slice().sort((a, b) => a - b);
            const mergesortedArray = SaveAnimationMergeSort(array.slice());
            console.log( arraysAreEqual( javaScriptsortedArray, mergesortedArray));
        }
    }

 
    render(){
        const{array} = this.state;

        return(
         <div className="array-container">
            {array.map ((value, idx) => (
                <div 
                    className="array-bar" 
                    key={idx}
                    style={{
                        height: `${value}px`,
                        
                    }}></div>
                ))}
            <input onChange={(e) => this.setState({sizeArray : e.target.value})}></input>
            <button onClick={() => this.resetArray()}>Generate Array</button>
            <button onClick={() => this.mergeSort()}>Merge Sort</button>
            <button onClick={() => this.quickSort()}>Quick Sort</button>
            </div>
        );
    }
}
    function randomIntFromInterval(min,max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
        
    }
    function arraysAreEqual (arrayOne, arrayTwo) {
        console.log('foo');
        if (arrayOne.length !== arrayTwo.length) return false;
        console.log('foo'); 
        for (let i = 0; i < arrayOne.length; i++) {
            if (arrayOne[i] !== arrayTwo[i]) {
                return false;
            }
        }
        return true;
    }